<?php
namespace Dinya\UnobtrusiveValidationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author      Peter Dinya<dinyapeti@gmail.com>
 */
class DinyaUnobtrusiveValidationBundle extends Bundle
{
}
