<?php

namespace Dinya\UnobtrusiveValidationBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Remote extends Constraint
{
    public $message = 'The string "{{ string }}" is invalid.';
    public $routeName;
    public $additionalFields = array();
    public $method;

    public function __construct(array $options)
    {
        if (isset($options['additionalFields'])){
            $this->additionalFields = (array) $options['additionalFields'];
            unset($options['additionalFields']);
        }
        parent::__construct($options);

    }

    public function getDefaultOption()
    {
        return 'routeName';
    }

    public function getRequiredOptions()
    {
        return ['routeName'];
    }


}