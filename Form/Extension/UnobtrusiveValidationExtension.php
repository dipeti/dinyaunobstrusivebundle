<?php

namespace Dinya\UnobtrusiveValidationBundle\Form\Extension;


use Dinya\UnobtrusiveValidationBundle\Util\UnobtrusiveValidationConstants;
use Dinya\UnobtrusiveValidationBundle\Validator\Constraints\Remote;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\CardScheme;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Exception\NoSuchMetadataException;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Mapping\MetadataInterface;
use Symfony\Component\Validator\Mapping\PropertyMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UnobtrusiveValidationExtension extends AbstractTypeExtension
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var null|string
     */
    protected $translationDomain;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param ValidatorInterface $validator
     * @param RouterInterface $router
     * @param TranslatorInterface $translator The translator for translating error messages
     * @param bool $translationDomain The translation domain for translating
     */
    public function __construct(ValidatorInterface $validator, RouterInterface $router, TranslatorInterface $translator = null, $translationDomain = false)
    {
        $this->validator = $validator;
        $this->router = $router;
        $this->translator = $translator;
        $this->translationDomain = $translationDomain;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(
            array(
                'label_translation_domain' => false,
                'translation_domain' => $this->translationDomain
            )
        )
            ->setAllowedTypes('label_translation_domain', array('null', 'string', 'boolean'))
            ->setAllowedTypes('translation_domain', array('null', 'string', 'boolean'));
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if ($this->isMainParentView($view))
        {
            $aggregatedConstraints = $this->aggregateConstraints($form);
            if (!empty($aggregatedConstraints))
            {
                foreach ($aggregatedConstraints as $propertyName => $constraints)
                {
                    $label = $propertyName;
                    $attributes = array();
                    foreach ($constraints as $constraint)
                    {
                        if($this->isValidationDisabled($constraint))
                        {
                            continue;
                        }
                        $attributes = $this->setValidationForField($attributes);
                        switch (get_class($constraint))
                        {
                            case 'Symfony\Component\Validator\Constraints\Required':
                                $attributes = $this->handleRequired($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\Bicf':
                                $constraint->htmlPattern = UnobtrusiveValidationConstants::BIC_REGEX_PATTERN;
                            case 'Symfony\Component\Validator\Constraints\Regex':
                                $attributes = $this->handleRegex($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\Range':
                                $attributes = $this->handleRange($constraint, $label, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\Blank':
                                $attributes = $this->handleBlank($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\NotBlank':
                                $attributes = $this->handleNotBlank($constraint, $attributes );
                                break;
                            case 'Symfony\Component\Validator\Constraints\Length':
                                $attributes = $this->handleLength($constraint, $attributes, $label);
                                break;
                            case 'Symfony\Component\Validator\Constraints\Type':
                                $attributes = $this->handleType($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\Date':
                                $attributes = $this->handleDate($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\Email':
                                $attributes = $this->handleEmail($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\CardScheme':
                                $attributes = $this->handleCardScheme($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\Url':
                                $attributes = $this->handleUrl($constraint, $attributes);
                                break;
                            case 'Symfony\Component\Validator\Constraints\File':
                                $attributes = $this->handleFile($constraint, $attributes);
                                break;
                            case 'Dinya\UnobtrusiveValidationBundle\Validator\Constraints\Remote':
                                $attributes = $this->handleRemote($view, $constraint, $attributes);
                                break;
                        }
                    }
                    $this->applyChanges($view, $label, $attributes);
                    }
                }
            }

    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return 'Symfony\Component\Form\Extension\Core\Type\FormType';
    }

    /**
     * Translates the given message.
     *
     * @param   string $id The message id (may also be an object that can be cast to string)
     * @param   int $number The number to use to find the indice of the message
     * @param   array $parameters An array of parameters for the message
     * @param   string $translationDomain
     * @return  string The translated string
     */
    protected function transChoice($id, $number, array $parameters = array(), $translationDomain = null) : string
    {
        $result = $id;

        if ($this->translator !== null)
        {
            $translationDomain = $translationDomain !== null ? $translationDomain : $this->translationDomain;

            if ($number >= 0)
            {
                $result = $this->translator->transChoice(
                    $id,
                    $number,
                    $parameters,
                    $translationDomain
                );
            }
            else
            {
                $result = $this->translator->trans(
                    $id,
                    $parameters,
                    $translationDomain
                );
            }
        }
        return $result;
    }

    protected function trans($id, array $parameters = array(), $translationDomain = null)
    {
        return $this->transChoice($id, 0, $parameters, $translationDomain);
    }

    protected function handleRadioButton($view,$label,$attributes)
    {
        if(isset($view->children[$label]->vars['expanded']))
        {
            foreach ($view->children[$label]->children as $child)
            {
                $child->vars['attr'] = $attributes;
            }
        }
    }

    /**
     * @param Required $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleRequired($constraint, $attributes)
    {
        $attributes['data-val-required'] = $this->trans('This field is required.');

        return $attributes;
    }

    /**
     * @param Regex $constraint
     * @param array $attributes
     * @return array
     */
    protected function handleRegex($constraint, $attributes) : array
    {
        $attributes['data-val-regex'] = $this->trans($constraint->message);
        $attributes['data-val-regex-pattern'] = $constraint->htmlPattern;

        return $attributes;
    }

    /**
     * @param Range $constraint
     * @param $label
     * @param array $attributes
     * @return array
     */
    protected function handleRange($constraint, $label, $attributes) : array
    {
        if (!is_null($constraint->min) && !is_null($constraint->max)) {
            $attributes['data-val-range'] = $this->trans(
                'The field \'{{ field_name }}\' value should be in range {{ min }} to {{ max }}.',
                array(
                    '{{ field_name }}' => $label,
                    '{{ min }}' => $constraint->min,
                    '{{ max }}' => $constraint->max
                )
            );
            $attributes['data-val-range-min'] = $constraint->min;
            $attributes['data-val-range-max'] = $constraint->max;
        }
        elseif (!is_null($constraint->min))
        {
            $attributes['data-val-range'] = $this->trans($constraint->minMessage,
                array(
                    '{{ field_name }}' => $label,
                    '{{ limit }}' => $constraint->min
                )
            );
            $attributes['data-val-range-min'] = $constraint->min;
        }
        else
        {
            $attributes['data-val-range'] = $this->trans($constraint->maxMessage,
                array(
                    '{{ field_name }}' => $label,
                    '{{ limit }}' => $constraint->max
                )
            );
            $attributes['data-val-range-max'] = $constraint->max;
        }

        return $attributes;
    }

    /**
     * @param Blank $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleBlank($constraint, $attributes)
    {
        $attributes['data-val-length-max'] = 0;
        $attributes['data-val-length'] = $this->trans($constraint->message);

        return $attributes;
    }

    /**
     * @param NotBlank $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleNotBlank($constraint, $attributes)
    {
        $attributes['data-val-length-min'] = 1;
        $attributes['data-val-length'] = $this->trans($constraint->message);

        return $attributes;
    }

    /**
     * @param Length $constraint
     * @param array $attributes
     * @param $label
     * @return mixed
     */
    protected function handleLength($constraint, $attributes, $label)
    {
        if (!is_null($constraint->min) && !is_null($constraint->max))
        {
            $message = $this->trans(
                'The field \'{{ field_name }}\' value should be in at least {{ min }} and at most {{ max }} character(s).',
                array(
                    '{{ field_name }}' => $label,
                    '{{ min }}' => $constraint->min,
                    '{{ max }}' => $constraint->max
                )
            );
            $attributes['data-val-required'] = $message;
            $attributes['data-val-length-min'] = $constraint->min;
            $attributes['data-val-length-max'] = $constraint->max;
            $attributes['data-val-length'] = $message;
        }
        elseif (!is_null($constraint->min)) {
            $message = $this->transChoice(
                $constraint->minMessage,
                $constraint->min,
                ['{{ limit }}' => $constraint->min]
            );
            $attributes['data-val-required'] = $message;
            $attributes['data-val-length-min'] = $constraint->min;
            $attributes['data-val-length'] = $message;
        }
        elseif (!is_null($constraint->max)) {
            $attributes['data-val-length-max'] = $constraint->max;
            $attributes['data-val-length'] = $this->transChoice(
                $constraint->maxMessage,
                $constraint->max,
                ['{{ limit }}' => $constraint->max]
            );
        }
        return $attributes;
    }

    /**
     * @param Type $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleType($constraint, $attributes)
    {
        switch ($constraint->type) {
            case 'integer':
            case 'int':
            case 'long':
                $attributes['data-val-digits'] = $this->trans($constraint->message);
                break;
            case 'float':
            case 'numeric':
            case 'real':
                $attributes['data-val-number'] = $this->trans($constraint->message);
                break;
        }

        return $attributes;
    }

    /**
     * @param Date $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleDate($constraint, $attributes)
    {
        $attributes['data-val-date'] = $this->trans($constraint->message);

        return $attributes;
    }

    /**
     * @param Email $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleEmail($constraint, $attributes)
    {
        $attributes['data-val-email'] = $this->trans($constraint->message);

        return $attributes;
    }

    /**
     * @param CardScheme $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleCardScheme($constraint, $attributes)
    {
        $attributes['data-val-creditcard'] = $this->trans($constraint->message);

        return $attributes;
    }

    /**
     * @param Url $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleUrl($constraint, $attributes)
    {
        $attributes['data-val-url'] = $this->trans($constraint->message);

        return $attributes;
    }

    /**
     * @param File $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleFile($constraint, $attributes)
    {
        $mimeTypes = implode(',', $constraint->mimeTypes);
        $attributes['data-val-accept-mimtype'] = $mimeTypes;
        $attributes['data-val-accept'] = $this->trans($constraint->mimeTypesMessage,
            [
                '{{ types }}' => $mimeTypes,
                ' ({{ type }})' => ''
            ]);

        return $attributes;
    }

    /**
     * @param FormView $view
     * @param Remote $constraint
     * @param array $attributes
     * @return mixed
     */
    protected function handleRemote(FormView $view, $constraint, $attributes)
    {
        $formattedFields = $this->filterAdditionalFieldsForRemote($constraint->additionalFields, $view);
        if (!empty($formattedFields)) {
            $attributes['data-val-remote-additionalfields'] = $formattedFields;
        }
        $attributes['data-val-remote'] = $this->trans($constraint->message);
        $attributes['data-val-remote-url'] = $this->router->generate($constraint->routeName);

        return $attributes;
    }

    /**
     * @param array $additionalFields
     * @param $view - the main parent view of the form
     * @return string - the name attribute of the input elements appended. Eg.: "form[firstName],form[age]"
     */
    private function filterAdditionalFieldsForRemote(array $additionalFields, $view)
    {
        $filteredFields = array();
        foreach ($view->children as $key => $viewItem)
        {
            if (in_array($key,$additionalFields,true))
            {
                $filteredFields[] = $viewItem->vars['full_name'];
            }
        }
        return implode(',', $filteredFields);
    }

    /**
     * @param $clazz
     * @return MetadataInterface
     */
    private function getMetaDataForClass($clazz)
    {
        try
        {
            if ($this->validator->hasMetadataFor($clazz))
            {
                return $this->validator->getMetadataFor($clazz);
            }
        }
        catch (NoSuchMetadataException $e)
        {
            dump('error');
        }
        return null;
    }

    /**
     * @param $metaData MetadataInterface|null
     * @return Constraint[]
     */
    protected function getConstraintsFromMetadata($metaData)
    {
        $classConstraints = array();
        if (!is_null($metaData))
        {
            /** @var $metaData ClassMetadata */
            $properties = $metaData->getConstrainedProperties();
            foreach ($properties as $property)
            {
                foreach ($metaData->getPropertyMetadata($property) as $propertyMetadata)
                {
                    /** @var $propertyMetadata PropertyMetadata*/
                    foreach ($propertyMetadata->getConstraints() as $constraint)
                    {
                        $classConstraints[$propertyMetadata->getPropertyName()][] = $constraint;
                    }
                }
            }
        }
        return $classConstraints;
    }

    /**
     * @param FormInterface $form
     * @param $classConstraints
     * @return Constraint[]
     */
    protected function retainRelevantConstraints(FormInterface $form, $classConstraints)
    {
        // Keep only those constraints that belong to a property contained in the current form
        $formElements = $form->all();
        $aggregatedConstraints = array_intersect_key($classConstraints, $formElements);
        // Collects the Constraints that are passed through at form creation.
        foreach ($formElements as $propertyName => $formElement)
        {
            $formTypeConstraints = $formElement->getConfig()->getOption('constraints',array());
            foreach ($formTypeConstraints as $formTypeConstraint)
            {
                // TODO: the same constraint is defined in metadata and upon form creation
                $aggregatedConstraints[$propertyName][] = $formTypeConstraint;
            }
        }
        return $aggregatedConstraints;
    }

    /**
     * @param FormInterface $form
     * @return Constraint[]
     */
    protected function aggregateConstraints(FormInterface $form)
    {
        $clazz = $form->getConfig()->getDataClass();
        $metaData = $this->getMetaDataForClass($clazz);
        $classConstraints = $this->getConstraintsFromMetadata($metaData);
        return $this->retainRelevantConstraints($form, $classConstraints);
    }

    /**
     * Only main Parent FormViews are processed
     * @param FormView $view
     * @return bool
     */
    private function isMainParentView(FormView $view)
    {
        return is_null($view->parent);
    }

    /**
     * @param FormView $view
     * @param $label
     * @param $attributes
     */
    protected function applyChanges(FormView $view, $label, $attributes)
    {
        $this->handleRadioButton($view, $label, $attributes);
        $view->children[$label]->vars['attr'] += $attributes;
    }

    /**
     * @param array $attributes
     * @param string $key
     * @param string $value
     * @return array
     */
    protected function setValidationForField(array $attributes, $key = 'data-val', $value = 'true') : array
    {
        $attributes[$key] = $value;
        return $attributes;
    }

    private function isValidationDisabled($constraint)
    {
        return (isset($constraint->payload[UnobtrusiveValidationConstants::VALIDATION_DISABLED]) &&
                $constraint->payload[UnobtrusiveValidationConstants::VALIDATION_DISABLED] !== false) ||
                (!is_array($constraint->payload) &&
                strpos($constraint->payload,UnobtrusiveValidationConstants::VALIDATION_DISABLED) !==false);
    }

}

