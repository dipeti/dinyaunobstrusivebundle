<?php

namespace Dinya\UnobtrusiveValidationBundle\Util;


class UnobtrusiveValidationConstants
{
    const BIC_REGEX_PATTERN = '([a-zA-Z]{4})([a-zA-Z]{2})(([2-9a-zA-Z]{1})([0-9a-np-zA-NP-Z]{1}))((([0-9a-wy-zA-WY-Z]{1})([0-9a-zA-Z]{2}))|([xX]{3})|)';
    const VALIDATION_DISABLED = 'unobtrusive-disabled';
}