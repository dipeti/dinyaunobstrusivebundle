<?php

namespace Dinya\UnobtrusiveValidationBundle\Tests\Form\Extension;


use Dinya\UnobtrusiveValidationBundle\Validator\Constraints\Remote;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\CardScheme;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UnobtrusiveValidationExtensionTest extends TypeTestCase
{

    const FIELD_NAME = "fieldName";
    const ROUTE_NAME = 'routeName';
    const ROUTE_PATH = '/routePath';
    private $translator;
    private $translationDomain;
    private $validator;
    private $router;

    public function setUp()
    {
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->translationDomain = 'validations';
        $this->router = $this->createMock(RouterInterface::class);
        parent::setUp();

    }

    protected function getExtensions()
    {
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->validator
            ->method('validate')
            ->will($this->returnValue(new ConstraintViolationList()));
        $this->validator
            ->method('getMetadataFor')
            ->will($this->returnValue(new ClassMetadata(Form::class)));
        return array(
            new ValidatorExtension($this->validator),
            new UnobtrusiveExtension($this->translator,$this->translationDomain,$this->validator,$this->router)
        );
    }


    public function testRequired()
    {
        $form = $this->createFormWithConstraint(new Required());
        $this->setUpTranslator('This field is required.');

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame('This field is required.',$expectedArray['data-val-required']);
    }

    public function testRegex()
    {
        $constraint = new Regex(['/^\w+/']);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-regex']);
        self::assertSame($constraint->htmlPattern,$expectedArray['data-val-regex-pattern']);
    }

    public function testRangeWithMinLimit()
    {
        $constraint = new Range(['min' => 1]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->minMessage);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertArrayNotHasKey('data-val-range-max',$expectedArray);
        self::assertSame($constraint->minMessage,$expectedArray['data-val-range']);
        self::assertSame($constraint->min,$expectedArray['data-val-range-min']);
    }

    public function testRangeWithMaxLimit()
    {
        $constraint = new Range(['max' => 1]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->maxMessage);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertArrayNotHasKey('data-val-range-min',$expectedArray);
        self::assertSame($constraint->maxMessage,$expectedArray['data-val-range']);
        self::assertSame($constraint->max,$expectedArray['data-val-range-max']);
    }

    public function testRangeWithMinAndMaxLimit()
    {
        $message = 'The field \'{{ field_name }}\' value should be in range {{ min }} to {{ max }}.';
        $constraint = new Range(['min'=> 1 ,'max' => 2]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($message,$expectedArray['data-val-range']);
        self::assertSame($constraint->max,$expectedArray['data-val-range-max']);
        self::assertSame($constraint->min,$expectedArray['data-val-range-min']);
    }

    public function testBlank()
    {
        $constraint = new Blank();
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertArrayHasKey('data-val',$expectedArray);
        self::assertSame($constraint->message,$expectedArray['data-val-length']);
        self::assertSame(0,$expectedArray['data-val-length-max']);
    }

    public function testNotBlank()
    {
        $constraint = new NotBlank();
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-length']);
        self::assertSame(1,$expectedArray['data-val-length-min']);
    }

    public function testLengthWithMinLimit()
    {
        $constraint = new Length(['min' => 1]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->minMessage);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertArrayNotHasKey('data-val-length-max', $expectedArray);
        self::assertSame($constraint->minMessage, $expectedArray['data-val-length']);
        self::assertSame($constraint->min, $expectedArray['data-val-length-min']);
    }

    public function testLengthWithMaxLimit()
    {
        $constraint = new Length(['max' => 1]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->maxMessage);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertArrayNotHasKey('data-val-length-min',$expectedArray);
        self::assertSame($constraint->maxMessage, $expectedArray['data-val-length']);
        self::assertSame($constraint->max, $expectedArray['data-val-length-max']);
    }

    public function testLengthWithMinAndMaxLimit()
    {
        $message = 'The field \'{{ field_name }}\' value should be in at least {{ min }} and at most {{ max }} character(s).';
        $constraint = new Length(['min'=> 1 ,'max' => 2]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($message,$expectedArray['data-val-length']);
        self::assertSame($constraint->max,$expectedArray['data-val-length-max']);
        self::assertSame($constraint->min,$expectedArray['data-val-length-min']);
    }

    public function testType()
    {
        $constraint = new Type(['type' => 'integer']);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-digits']);
    }

    public function testDate()
    {
        $constraint = new Date();
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-date']);
    }

    public function testEmail()
    {
        $constraint = new Email();
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-email']);
    }

    public function testCardScheme()
    {
        $constraint = new CardScheme(['schemes' => 'VISA']);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-creditcard']);
    }

    public function testUrl()
    {
        $constraint = new Url();
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-url']);
    }

    public function testFile()
    {
        $constraint = new File(['mimeTypes' => ['application/pdf','application/x-pdf']]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->mimeTypesMessage);

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame('application/pdf,application/x-pdf',$expectedArray['data-val-accept-mimtype']);
        self::assertSame($constraint->mimeTypesMessage,$expectedArray['data-val-accept']);
    }

    public function testRemote()
    {
        $constraint = new Remote(['routeName' => self::ROUTE_NAME]);
        $form = $this->createFormWithConstraint($constraint);
        $this->setUpTranslator($constraint->message);
        $this->setUpRouter();

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertSame($constraint->message,$expectedArray['data-val-remote']);
        self::assertSame(self::ROUTE_PATH,$expectedArray['data-val-remote-url']);
    }

    public function testRadioButtonTWithConstraint()
    {
        $message = 'The field \'{{ field_name }}\' value should be in range {{ min }} to {{ max }}.';
        $constraint = new Range(['min'=> 1 ,'max' => 2]);
        $form = $this->createRadioBtnFormAndWithConstraint($constraint);
        $this->setUpTranslator($message);
        $this->setUpRouter();

        $formView = $form->createView();
        $firstExpectedArray = $formView->children[self::FIELD_NAME]->children[0]->vars['attr'];
        $secondExpectedArray = $formView->children[self::FIELD_NAME]->children[1]->vars['attr'];

        self::assertSame($message,$firstExpectedArray['data-val-range']);
        self::assertSame($message,$secondExpectedArray['data-val-range']);
        self::assertSame($constraint->max,$firstExpectedArray['data-val-range-max']);
        self::assertSame($constraint->max,$secondExpectedArray['data-val-range-max']);
        self::assertSame($constraint->min,$firstExpectedArray['data-val-range-min']);
        self::assertSame($constraint->min,$secondExpectedArray['data-val-range-min']);
    }

    public function testWhenConstraintIsDisabled()
    {
        $form = $this->createFormWithConstraint(new Required(['payload' => ['unobtrusive-disabled' => true]]));
        $this->setUpTranslator('This field is required.');

        $formView = $form->createView();
        $expectedArray = $formView->children[self::FIELD_NAME]->vars['attr'];

        self::assertArrayNotHasKey('data-val',$expectedArray);
        self::assertArrayNotHasKey('data-val-required',$expectedArray);
    }

    /**
     * @param Constraint $constraint
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createFormWithConstraint(Constraint $constraint)
    {
        return $this->factory->createBuilder()
            ->add(
                self::FIELD_NAME,TextType::class,
                [
                    'constraints' => [$constraint]
                ]
            )
            ->getForm();

    }

    /**
     * @param Constraint $constraint
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createRadioBtnFormAndWithConstraint(Constraint $constraint)
    {
        return $this->factory->createBuilder()
            ->add(
                self::FIELD_NAME,ChoiceType::class,
                [
                    'constraints' => [$constraint],
                    'expanded' => true,
                    'multiple' => false,
                    'choices' => [1 => 1, 2 => 2]
                ]
            )
            ->getForm();

    }

    private function setUpTranslator($message)
    {
        $this->translator->method('trans')->willReturn($message);
        $this->translator->method('transChoice')->willReturn($message);
    }

    private function setUpRouter()
    {
        $this->router->method('generate')->willReturn(self::ROUTE_PATH);
    }

}