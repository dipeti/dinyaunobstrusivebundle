<?php

namespace Dinya\UnobtrusiveValidationBundle\Tests\Form\Extension;


use Dinya\UnobtrusiveValidationBundle\Form\Extension\UnobtrusiveValidationExtension;
use Symfony\Component\Form\AbstractExtension;

class UnobtrusiveExtension extends AbstractExtension
{
    private $translator;
    private $translationDomain;
    private $validator;
    private $router;

    /**
     * UnobtrusiveExtension constructor.
     * @param $translator
     * @param $translationDomain
     * @param $validator
     * @param $router
     */
    public function __construct($translator, $translationDomain, $validator, $router)
    {
        $this->translator = $translator;
        $this->translationDomain = $translationDomain;
        $this->validator = $validator;
        $this->router = $router;
    }

    protected function loadTypeExtensions()
    {
        return array(
            new UnobtrusiveValidationExtension($this->validator,$this->router,$this->translator,$this->translationDomain)
        );
    }


}