<?php
/**
 * Created by PhpStorm.
 * User: dipet
 * Date: 2018. 04. 15.
 * Time: 21:48
 */

namespace Dinya\UnobtrusiveValidationBundle\Tests\Templating\Twig\Extension;

use Dinya\UnobtrusiveValidationBundle\Templating\Twig\Extension\ValidationSummaryExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormView;

class ValidationSummaryExtensionTest extends TestCase
{
    const EXPECTED_HTML = '<div data-valmsg-summary="true" class="class1 class2" id="name" ><ul></ul></div>';

    public function testGeneratedHtml()
    {
        // Arrange
        $form = new FormView();
        $form->vars['full_name'] = 'name';
        $extension = new ValidationSummaryExtension();
        $expectedHtml = self::EXPECTED_HTML;

        // Act
        $actualHtml = $extension->renderValidationError($form, ['attr' => [
            'class' => 'class1 class2',
            'id' => 'name'
        ]]);

        // Assert
        $this->assertSame($expectedHtml, $actualHtml);
    }
}