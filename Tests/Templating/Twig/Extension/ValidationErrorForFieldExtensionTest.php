<?php

namespace Dinya\UnobtrusiveValidationBundle\Tests\Templating\Twig\Extension;


use Dinya\UnobtrusiveValidationBundle\Templating\Twig\Extension\ValidationErrorForFieldExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormView;

class ValidationErrorForFieldExtensionTest extends TestCase
{
    const EXPECTED_HTML = '<span data-valmsg-for="name" data-valmsg-replace="true" class="class1 class2" id="name" ></span>';

    public function testGeneratedHtml()
    {
        // Arrange
        $form = new FormView();
        $form->vars['full_name'] = 'name';
        $extension = new ValidationErrorForFieldExtension();
        $expectedHtml = self::EXPECTED_HTML;

        // Act
        $actualHtml = $extension->renderValidationError($form, ['attr' => [
            'class' => 'class1 class2',
            'id' => 'name'
        ]]);

        // Assert
        $this->assertSame($expectedHtml, $actualHtml);
    }



}