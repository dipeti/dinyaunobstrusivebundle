<?php

namespace Dinya\UnobtrusiveValidationBundle\Templating\Twig\Extension;


use Symfony\Component\Form\FormView;

/*
 * Twig extension {{ form_validation_field(form) }}
 * Make sure to override form theme template!!!
 * Recommended to add it to {{ form_row }} such that
 * {% block form_row -%}
 *   <div>
 *       {{- form_label(form) -}}
 *       {{- form_widget(form) -}}
 *       {{- form_errors(form) -}}
 *       {{ form_validation_field(form) }}
 *   </div>
 * {%- endblock form_row %}
 */
class ValidationErrorForFieldExtension extends ValidationErrorExtensionBase
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'form_validation_field';
    }

    protected function getHtml(FormView $form, $attributes)
    {
        $field = $form->vars['full_name'];
        return "<span data-valmsg-for=\"{$field}\" data-valmsg-replace=\"true\" {$attributes}></span>";
    }
}