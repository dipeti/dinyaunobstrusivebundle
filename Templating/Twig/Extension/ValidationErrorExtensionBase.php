<?php

namespace Dinya\UnobtrusiveValidationBundle\Templating\Twig\Extension;


use Symfony\Component\Form\FormView;

abstract class ValidationErrorExtensionBase extends \Twig_Extension
{
    protected abstract function getHtml(FormView $form, $attributes);
    protected abstract function getName();
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction($this->getName(), [$this, 'renderValidationError'], ['is_safe' => array('html')])
        ];
    }

    public function renderValidationError(FormView $form = null, $options = array())
    {
        $attributes = $this->generateAttributes($options);
        return $this->getHtml($form, $attributes);
    }

    protected function generateAttributes($options)
    {
        $attributes = '';
        if (isset($options['attr']))
        {
            foreach ($options['attr'] as $name => $value)
            {
                $attributes = sprintf("%s%s=\"%s\" ", $attributes, $name, $value);
            }
        }
        return $attributes;
    }
}