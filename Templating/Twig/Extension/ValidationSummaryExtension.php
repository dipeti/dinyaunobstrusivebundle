<?php

namespace Dinya\UnobtrusiveValidationBundle\Templating\Twig\Extension;


use Symfony\Component\Form\FormView;

class ValidationSummaryExtension extends ValidationErrorExtensionBase
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'form_validation_summary';
    }


    protected function getHtml(FormView $form, $attributes)
    {
        return "<div data-valmsg-summary=\"true\" {$attributes}><ul></ul></div>";
    }
}